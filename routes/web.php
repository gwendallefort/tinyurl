<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RedirectionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', [RedirectionController::class , 'pageHome'])->middleware(['auth', 'verified'])->name('home');

// Redirect tiny url
Route::get('/r/{id}', [RedirectionController::class, 'redirect'])->name('redirect');

// Require authentification
Route::middleware('auth')->group(function () {

    // pages
    Route::get('/addRedirection', [RedirectionController::class , 'pageStore'])->name('page.storeRedirection');
    Route::get('/updateRedirection/{id}', [RedirectionController::class , 'pageUpdate'])->name('page.updateRedirection');

    // redirections
    Route::post('/addRedirection', [RedirectionController::class, 'store'])->name('redirections.store');
    Route::put('/updateRedirection/{id}', [RedirectionController::class, 'update'])->name('redirections.update');
    Route::post("/deleteRedirection/{id}", [RedirectionController::class, 'delete'])->name('redirections.delete');

    // profile
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
