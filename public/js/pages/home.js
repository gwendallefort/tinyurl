
function setClipboard(txt) {
    const type = "text/plain";
    const blob = new Blob([txt], { type });
    const data = [new ClipboardItem({ [type]: blob })];
    navigator.clipboard.write(data).then(() => {
            let x = document.getElementById("snackbar");
            x.className = "show";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 1500);
        }
    );
}