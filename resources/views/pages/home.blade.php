@extends('master')

@section('style')
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    <link href="{{ asset('css/redirection.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/buttonPageSize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/button.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/snackbar.css') }}" rel="stylesheet">
@endsection

@section('script')
    <script src="{{ asset('js/table-sort.js') }}"></script>
    <script src="{{ asset('js/pages/home.js') }}"></script>
@endsection

@section('content')

    <div class="p-6">

        <p class="title">Liste des Alias</p>

        <p class="info">Les alias sont sensible à la case.</p>

        <!-- Recherche -->
        <div id="search">
            <form 
                method="GET" 
                action="{{route('home')}}"
                >
                <label for="search">Recherche</label>
                <div>
                    <input type="text" name="search" value="{{app('request')->query('search')}}"></input>
                    <input type="text" name="page" style="display:none;" value="{{1}}"></input>
                    <input type="text" name="size" style="display:none;" value="{{app('request')->query('size')}}"></input>
                    @include('components.button')
                </div>
            </form>
            <a href="{{route('home')}}">
                <button type="submit">Réinitialiser</button>
            </a>
        </div>

        <!-- Pagination -->
        <div id="pagination">

            <p>Nombre d'éléments par page</p>
            <div>
                @include('components.buttonPageSize', ['size' => '20'])
                @include('components.buttonPageSize', ['size' => '50'])
                @include('components.buttonPageSize', ['size' => '100'])
                @include('components.buttonPageSize', ['size' => '250'])
            </div>

            {{ 
                $redirections
                    ->onEachSide(1)
                    ->links('vendor/pagination/tailwind', [
                        'size' => app('request')->query('size'),
                        'search' => app('request')->query('search'),
                    ]) 
            }}

        </div>

        <!-- Alias -->
        <table class="table-sort table-arrows">
            <thead>
                <tr>
                    <th>Url</th>
                    <th>Alias</th>
                    <th colspan="3">Utilisations</th>
                </tr>
                <colgroup>
                    <col style="width:50%">
                    <col style="width:30%">
                    <col style="width:10%">
                    <col style="width:40px">
                    <col style="width:40px">
                </colgroup> 
            </thead>
            <tbody>
                @foreach ($redirections as $redirection)
                    @include('components.redirection', [$redirection])
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Snackbar -->
    <div id="snackbar">Copié dans le presse papier.</div>

@endsection
