@extends('master')


@section('style')
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/button.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="p-6">

        <p class="title">Create a new Redirection</p>

        @if (session('errorMessage'))
            <p class="errorMessage">{{ session('errorMessage') }}</p>
        @endif

        @include('components.formRedirection', [
            'redirection' => null,
            'method' => 'POST',
            'routeName' => "redirections.store",
        ])

    </div>

@endsection
