@extends('master')


@section('style')
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/button.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="p-6">

        <p class="title">Update a Redirection</p>
        
        @if (session('errorMessage'))
            <p class="errorMessage">{{ session('errorMessage') }}</p>
        @endif

        @include('components.formRedirection', [
            'redirection' => $redirection,
            'method' => 'PUT',
            'routeName' => "redirections.update",
            'routeParams' => ["id" => $redirection->id],
        ])

    </div>

@endsection
