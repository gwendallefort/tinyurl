<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tiny Url</title>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <script src="https://kit.fontawesome.com/d678efe89e.js" crossorigin="anonymous"></script>
    @yield('script')

    <!-- Style -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
    @yield('style')

</head>
<body>

    @include('layouts.navigation')

    @yield('content')

</body>
</html>
