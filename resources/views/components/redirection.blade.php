

<tr>
    <td>{{ $redirection->baseUrl }}</td>
    <td class="columnAlias">
        <div>
            {{ $url = url('') . "/r/" . $redirection->tinyUrl }}
        </div>
        <button class="btnCopy" onclick="setClipboard('{{$url}}')">Copy</button>
    </td>
    <td class="text-center">{{ $redirection->counter->count ?? 0 }}</td>
    <!-- Update -->
    <td class="redirectionBtn">
        <span>
            <a href="{{ route('page.updateRedirection', $redirection->id) }}"><i class="fas fa-edit"></i></a>
        </span>
    </td>
     <!-- Delete -->
    <td class="redirectionBtn">
        <span>
            <form 
                method="POST" 
                action="{{ route('redirections.delete', $redirection->id) }}"
                onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer l\'alias ?');"
            >
                @csrf
                <button type="submit">
                    <i class="fas fa-trash"></i>
                </button>
            </form>
        </span>
    </td>
</tr>