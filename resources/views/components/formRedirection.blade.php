<form method="POST" action="{{ route($routeName, $routeParams ?? '') }}">
    @csrf

    @if ($method === "PUT")
        @method('PUT')
    @endif

    <div>
        <label for="baseUrl">Url</label>
        <input required maxlength="1000" type="text" name="baseUrl"  value="{{ $redirection->baseUrl ?? '' }}">
    </div>

    <div>
        <label for="tinyUrl">Alias ID (Optionnel)</label>
        <input maxlength="300" type="text" name="tinyUrl" value="{{ $redirection->tinyUrl ?? '' }}">
    </div>

    @include('components.button')

</form>