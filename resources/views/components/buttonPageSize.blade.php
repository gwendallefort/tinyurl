<div 
    class="
    buttonPageSize 
    {{ app('request')->input('size') ==  $size ? 'buttonPageSizeActive' : ''  }}"
>
    <a href="{{route('home', [
        'page'=> 1, 
        'size' => $size, 
        'search' => app('request')->query('search')]); 
    }}"
    >{{$size}}</a>
</div>