<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends BaseTestCase
{

    use CreatesApplication;
    use WithoutMiddleware;

    // rollback change in db after each test
    public function setUp(): void { // avant chaque test
        parent::setUp();
        DB::beginTransaction();
    }
    public function tearDown(): void { // après chaque test
        DB::rollback();
        parent::tearDown();
    }

    use CreatesApplication;
}
