<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Redirection;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RedirectionStoreTest extends TestCase
{

    public function test_redirection_store(): void
    {
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->post('/addRedirection', [
                'baseUrl' => 'test_baseUrl',
                'tinyUrl' => 'test_tinyUrl',
            ]);

        // Assertions
        $response->assertStatus(302);

        $this->assertDatabaseHas("redirections", [
            'baseUrl' => 'test_baseUrl',
            'tinyUrl' => 'test_tinyUrl',
            'user_id' => $user->id,
        ]);

    }

    public function test_redirection_store_without_tinyUrl(): void
    {
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->post('/addRedirection', [
                'baseUrl' => 'test_baseUrl',
                'tinyUrl' => null,
            ]);

        // Assertions
        $response->assertStatus(302);

        $this->assertDatabaseHas("redirections", [
            'baseUrl' => 'test_baseUrl',
            'user_id' => $user->id,
        ]);

    }

    public function test_redirection_store_alias_id_not_unique(): void
    {
        Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => 1,
        ]);

        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->post('/addRedirection', [
                'baseUrl' => 'test_baseUrl',
                'tinyUrl' => 'test_tinyUrl',
            ]);


        // Assertions
        $response->assertSessionHas("errorMessage", "Cet Alias ID existe déjà.");

        $this->assertDatabaseMissing("redirections", [
            'baseUrl' => 'test_baseUrl',
            'tinyUrl' => 'test_tinyUrl',
            'user_id' => $user->id,
        ]);
    }
}