<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Redirection;

use Illuminate\Foundation\Testing\RefreshDatabase;


class RedirectionRedirectTest extends TestCase
{
    public function test_redirection_redirect_and_counter_is_created(): void
    {
        $redirection = Redirection::create([
            'baseUrl'=> "https://www.google.com/",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => 1,
        ]);

        $response = $this->get('/r/test_tinyUrl');

        // Assertions
        $response
            ->assertStatus(302)
            ->assertRedirect('https://www.google.com/');

        $this->assertDatabaseHas("redirection_counts", [
            'count' => 1,
            'redirection_id' => $redirection->id,
        ]);
    }

    public function test_redirection_redirect_not_found(): void
    {
        $response = $this->get('/r/test_tinyUrl');

        // Assertions
        $response->assertStatus(404);
    }
}