<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Redirection;

use DB;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RedirectionUpdateTest extends TestCase
{

    public function test_redirection_update(): void
    {
        $user = User::factory()->create();

        $redirection = Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => $user->id,
        ]);

        $response = $this
            ->actingAs($user)
            ->put('/updateRedirection/'.$redirection->id, [
                'baseUrl' => 'test_baseUrl',
                'tinyUrl' => 'test_tinyUrl_updated',
            ]);

        // Assertions
        $response->assertStatus(302);

        $this->assertDatabaseHas("redirections", [
            'baseUrl' => 'test_baseUrl',
            'tinyUrl' => 'test_tinyUrl_updated',
            'user_id' => $user->id,
        ]);

    }

    public function test_redirection_update_dont_belong_to_user(): void
    {
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->put('/updateRedirection/1', [
                'baseUrl' => 'test_baseUrl',
                'tinyUrl' => 'test_tinyUrl',
            ]);

        // Assertions
        $response->assertSessionHas("errorMessage", "Erreur lors de la mise à jour de la redirection.");

        $this->assertDatabaseMissing("redirections", [
            'baseUrl' => 'test_baseUrl',
            'tinyUrl' => 'test_tinyUrl',
            'user_id' => $user->id,
        ]);
    }

    public function test_redirection_update_not_found(): void
    {
        $user = User::factory()->create();

        $id=DB::select("SHOW TABLE STATUS LIKE 'redirections'");
        $non_existent_id=$id[0]->Auto_increment;

        $response = $this
            ->actingAs($user)
            ->put('/updateRedirection/'.$non_existent_id, [
                'baseUrl' => 'test_baseUrl',
                'tinyUrl' => 'test_tinyUrl',
            ]);

        // Assertions
        $response->assertStatus(404);

        $this->assertDatabaseMissing("redirections", [
            'baseUrl' => 'test_baseUrl',
            'tinyUrl' => 'test_tinyUrl',
            'user_id' => $user->id,
        ]);
    }

    public function test_redirection_update_alias_id_not_unique(): void
    {
        $user = User::factory()->create();

        Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => $user->id,
        ]);

        $redirection = Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUr",
            'user_id' => $user->id,
        ]);

        $response = $this
            ->actingAs($user)
            ->put('/updateRedirection/'.$redirection->id, [
                'baseUrl' => "test_baseUrl",
                'tinyUrl' => 'test_tinyUrl',
            ]);

        // Assertions
        $response->assertSessionHas("errorMessage", "Cet Alias ID existe déjà.");
    }
}