<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Redirection;

use DB;
use Illuminate\Foundation\Testing\RefreshDatabase;


class RedirectionDeleteTest extends TestCase
{

    public function test_redirection_delete(): void
    {
        $user = User::factory()->create();

        $redirection = Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => $user->id,
        ]);

        $response = $this
            ->actingAs($user)
            ->post('/deleteRedirection/'.$redirection->id);

        // Assertions
        $response->assertStatus(302);

        $this->assertDatabaseMissing("redirections", [
            'baseUrl' => 'test_baseUrl',
            'tinyUrl' => 'test_tinyUrl',
            'user_id' => $user->id,
        ]);

    }

    public function test_redirection_delete_not_found(): void
    {
        $user = User::factory()->create();

        $redirection = Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => $user->id,
        ]);

        $id=DB::select("SHOW TABLE STATUS LIKE 'redirections'");
        $non_existent_id=$id[0]->Auto_increment;

        $response = $this
            ->actingAs($user)
            ->post('/deleteRedirection/'.$non_existent_id);

        // Assertions
        $response->assertStatus(404);
    }

    public function test_redirection_delete_dont_belong_to_user(): void
    {
        $user = User::factory()->create();

        $redirection = Redirection::create([
            'baseUrl'=> "test_baseUrl",
            'tinyUrl' => "test_tinyUrl",
            'user_id' => $user->id,
        ]);

        $response = $this
            ->actingAs($user)
            ->post('/deleteRedirection/1');

        // Assertions
        $response->assertStatus(403);
    }
}