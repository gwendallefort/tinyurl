# Comment lancer le projet

Installer [Laragon](https://laragon.org/download/index.html).


## <ins> Installation du projet </ins>

```
cd C:\laragon\www\
git clone https://gitlab.com/gwendallefort/tinyurl.git
```

Installer les dépendances de Composer
```
cd tinyurl
composer install
```

Installer les dépendances de NPM
```
npm install
```

Générer l'app key
```
php artisan key:generate
```


## <ins> Database </ins>

<p>Créer une base de données MySql</p>
<p>Mettre a jour le .env en accord avec votre base de données</p>
<p>Migration et Seeder de la base de données :</p>

```
php artisan migrate:fresh --seed
```

## <ins> Lancer le projet </ins>

<p>Lancer Laragon</p>

[https://tinyurl.com](https://tinyurl.com/)

OU

```
php artisan serve
```


# Comment lancer les TU

```
php artisan test --filter test_redirection
```