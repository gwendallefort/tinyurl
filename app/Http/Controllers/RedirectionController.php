<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Models\Redirection;
use App\Models\RedirectionCount;
use App\Http\Controllers\Controller;


class RedirectionController extends Controller
{

    /**
     * Redirect tinyUrl to baseUrl
     */
    public function redirect(Request $request, $tinyUrl)
    {
        try {

            // get redirection - case sensitive
            $redirection = Redirection::whereRaw("BINARY `tinyUrl`= ?",[$tinyUrl])->with('counter')->first();

            if($redirection === null) 
                return abort(404);

            // if first redirection -> create counter
            if($redirection->counter === null) {
                RedirectionCount::create([
                    'count'=> 1,
                    'redirection_id' => $redirection->id,
                ]);
            } else {
                // increment counter
                $redirection->counter->count++;
                $redirection->counter->update();
            }

        } catch (NotFoundHttpException $e) {
            return abort(404);
        } catch (\Exception $e) {
            return redirect(route("home"));
        }

        return  redirect($redirection->baseUrl);
    }

    /**
     * Créer un élément
     */
    public function store(Request $request)
    {
        try {

            // check if data are valide
            $data = $request->validate([
                'baseUrl' => 'required|max:1000',
                'tinyUrl' => 'max:300',
            ]);

            $data["user_id"] = auth()->user()->id;

            // generate unique url if not set
            if($data["tinyUrl"] === null) {
                $id=DB::select("SHOW TABLE STATUS LIKE 'redirections'");
                $next_id=$id[0]->Auto_increment;
                $data["tinyUrl"] = $this->randomString();
            }
                
            // Create the redirection
            $redirection = Redirection::create($data);
            $redirection->save();

        } catch (\Exception $e) {
            $errorMessage = "Erreur lors de la mise à jour de la redirection.";

            if($e->getCode() == 23000) {
                $errorMessage = "Cet Alias ID existe déjà.";
            }

            return redirect()->route('page.storeRedirection')->with('errorMessage', $errorMessage);
        }

        return redirect(route("home"));
    }


    /**
     * Mets à jour un élément
     */
    public function update(Request $request, $id)
    {
        try {

            // check if data are valide
            $data = $request->validate([
                'baseUrl' => 'filled|max:1000',
                'tinyUrl' => 'filled|max:300',
            ]);
            
            $redirection = Redirection::find($id);

            if($redirection === null) 
                return abort(404);

            if($redirection->user_id != auth()->user()->id)
                return abort(403);

            // update the redirection
            $redirection->update($data);

        } catch (NotFoundHttpException $e) {
            return abort(404);
        } catch (\Exception $e) {
            $errorMessage = "Erreur lors de la mise à jour de la redirection.";

            if($e->getCode() == 23000) {
                $errorMessage = "Cet Alias ID existe déjà.";
            }

            return redirect()->route('page.updateRedirection', $id)->with([
                "errorMessage" => $errorMessage,
            ]);
        }

        return redirect(route("home"));
    }

    /**
     * Affiche la page home
     */
    public function pageHome(Request $request)
    {
        // get query params
        $size = $request->query('size') ?? 20;
        $search = $request->query('search');

        // set page max size
        $size = min($size, 100);

        // 
        $query = Redirection::where("user_id", auth()->user()->id);

        if($search != null) {
            $query->where('baseUrl', 'LIKE', '%'.$search.'%')
                ->orWhere('tinyUrl', 'LIKE', '%'.$search.'%');
        }

        $redirections = $query->paginate($size);

        return view('pages/home' , ["redirections" => $redirections]);
    }

    /**
     * Affiche la page de création d'une redirections
     */
    public function pageStore()
    {
        return view('pages/storeRedirection');
    }

    /**
     * Affiche la page de mise à jour d'une redirections
     */
    public function pageUpdate(Request $request, $id)
    {
        $redirection = Redirection::find($id);

        if($redirection === null) 
            return redirect(route("home"));

        if($redirection->user_id != auth()->user()->id)
            return redirect(route("home"));

        return view('pages/updateRedirection', ["redirection" => $redirection]);
    }
    
    /**
     * Supprime l'élément
     */
    public function delete($id) {

        $redirection = Redirection::find($id);
        if($redirection === null) 
            return abort(404);
        if($redirection->user_id != auth()->user()->id)
            return abort(403);

        Redirection::destroy($id);

        return redirect(route("home"));
    }

    /**
     * generate randon string (56 Mrd combinaisons)
     */
    function randomString($len = 6){
        $seed = str_split('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, $len) as $k) $rand .= $seed[$k];
        return $rand;
      }

}
