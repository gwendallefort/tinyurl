<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=> "admin",
            'email' => "a@a",
            'password' => Hash::make("12345678"),
        ]);

        for($i=0; $i<5; $i++) {
            User::create([
                'name'=> "user".$i,
                'email' => "user".$i."@mail",
                'password' => Hash::make("12345678"),
            ]);
        }
    }
}
