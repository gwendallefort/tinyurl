<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\RedirectionCount;

class RedirectionCountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RedirectionCount::create([
            'count'=> 5,
            'redirection_id' => 1,
        ]);
    }
}
