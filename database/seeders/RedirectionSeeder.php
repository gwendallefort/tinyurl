<?php

namespace Database\Seeders;

use App\Models\Redirection;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RedirectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<5; $i++) {
            Redirection::create([
                'baseUrl'=> "https://www.google.com/",
                'tinyUrl' => "tiny".$i,
                'user_id' => 1,
            ]);
        }
    }
}
